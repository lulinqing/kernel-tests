# storage/nvdimm/fsdax_presence_media_errors

Storage: nvdimm feature test for enable DAX in the presence of media errors, from BZ1383825

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
